#!/usr/bin/env bash
while true
do
	echo "Exec /usr/bin/curator --config /etc/curator/config_file.yml /etc/curator/action_file.yml;"
	set +
	/usr/bin/curator --config /etc/curator/config_file.yml /etc/curator/action_file.yml || (echo "Executed /usr/bin/curator --config /etc/curator/config_file.yml /etc/curator/action_file.yml")
	echo "Sleeping  for ${SLEEP_DELAY:-12h}";
	sleep ${SLEEP_DELAY:-12h};
done