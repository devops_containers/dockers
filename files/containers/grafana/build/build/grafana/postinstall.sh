#!/bin/bash -e
echo "${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS}"
if [ ! -d "${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS}" ]; then
    mkdir "${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS}"
fi


if [ ! -d "${GF_PATHS_PROVISIONING_OVERWRITE:-/var/lib/grafana-provisioning}" ]; then
    mkdir "${GF_PATHS_PROVISIONING_OVERWRITE:-/var/lib/grafana-provisioning}"
fi

if [ ! -z "${GF_INSTALL_PLUGINS}" ]; then
  OLDIFS=$IFS
  IFS=','
  for plugin in ${GF_INSTALL_PLUGINS}; do
    IFS=$OLDIFS
    if [[ $plugin =~ .*\;.* ]]; then
        pluginUrl=$(echo "$plugin" | cut -d';' -f 1)
        pluginInstallFolder=$(echo "$plugin" | cut -d';' -f 2)
        grafana-cli --pluginUrl ${pluginUrl} --pluginsDir "${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS}" plugins install "${pluginInstallFolder}"
    else
        grafana-cli --pluginsDir "${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS}" plugins install ${plugin}
    fi
  done
fi

apt-get update && apt-get install -y --no-install-recommends rsync git
mkdir -p /git/gitlab/cdn72/provisioning
git clone --depth 1  --branch master https://gitlab.com/cdn72/provisioning.git /git/gitlab/cdn72/provisioning
echo "Exec rsync -r /git/gitlab/cdn72/provisioning/files/grafana/provisioning/ ${GF_PATHS_PROVISIONING_OVERWRITE:-/var/lib/grafana-provisioning}/"
rsync -r /git/gitlab/cdn72/provisioning/files/grafana/provisioning/ ${GF_PATHS_PROVISIONING_OVERWRITE:-/var/lib/grafana-provisioning}/
rm -rf /git  /var/lib/apt/lists/*
chown -Rv ${GF_UID}:${GF_GID} ${GF_PATHS_PLUGINS_OVERWRITE:-$GF_PATHS_PLUGINS} ${GF_PATHS_PROVISIONING_OVERWRITE:-/var/lib/grafana-provisioning}
apt autoremove -y && apt purge -y rsync git 

du -sh ${GF_PATHS_PLUGINS_OVERWRITE}
du -sh ${GF_PATHS_PROVISIONING_OVERWRITE}
