#!/usr/bin/env python3.8
from selenium import webdriver
import os
from selenium.common.exceptions import WebDriverException
import dotenv

dotenv.load_dotenv()
try:
    browser = webdriver.Remote(
        command_executor="http://%s:%s" % (os.getenv("SELENIUM_HOST", "127.0.0.1"), os.getenv("SELENIUM_PORT", "4444")),
    )
    browser.close()
except WebDriverException as error:
    print(error)
    exit(1)
exit(0)

#python3.8 -m pip install selenium python-dotenv