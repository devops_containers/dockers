#!/usr/bin/env bash

if [[ -f "/selenium-check.py" ]];then
	python3.8 /selenium-check.py
	if [[ $? == "0" ]];then
		echo "Alive"
	else
		echo "Bad health" && kill  1
	fi
fi

