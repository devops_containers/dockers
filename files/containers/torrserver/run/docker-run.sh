#!/usr/bin/env bash

docker run --rm -d --name torrserver -v /mnt/hdd/ts:/opt/ts -p 8090:8090 ghcr.io/yourok/torrserver:latest